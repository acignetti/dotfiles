" FileTypeConfig {{{
setlocal foldmethod=indent
setlocal tabstop=8
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal textwidth=120
setlocal smarttab
setlocal expandtab

filetype plugin indent on
" }}}
