set nocompatible
filetype off

" Spaces & Tabs {{{
set encoding=utf-8
let mapleader=","
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set modelines=1
set fileformat=unix
set backspace=2
set autoindent
" }}}

" UI Layout {{{
set re=1
set colorcolumn=80,100,120
set lazyredraw
set ai
set si
set number              " show line numbers
set showcmd             " show command in bottom bar
set nocursorline        " highlight current line
set wildmenu
set cursorline
set showmatch           " higlight matching parenthesis
set relativenumber
" }}}

" Searching {{{
set tags=./tags
set ignorecase          " ignore case when searching
set incsearch           " search as characters are entered
set hlsearch            " highlight all matches
nnoremap <leader><space> :nohlsearch<CR>
" }}}

" Folding {{{
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
nnoremap <space> za
set foldlevelstart=10   " start with fold level of 1
" }}}

" Backups {{{
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup
" }}}

" Plug {{{
call plug#begin('~/.vim/plugged')
Plug 'AndrewRadev/splitjoin.vim'
Plug 'Shougo/context_filetype.vim'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'airblade/vim-gitgutter'
Plug 'psf/black', { 'tag': '19.10b0' }
Plug 'ap/vim-css-color'
Plug 'bling/vim-bufferline'
Plug 'easymotion/vim-easymotion'
Plug 'davidhalter/jedi-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'fisadev/vim-isort'
Plug 'flazz/vim-colorschemes'
Plug 'fmoralesc/vim-pad'
Plug 'habamax/vim-godot'
Plug 'haya14busa/incsearch-easymotion.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'
Plug 'haya14busa/incsearch.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'jremmen/vim-ripgrep'
Plug 'jreybert/vimagit'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/seoul256.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'mhinz/vim-startify'
Plug 'ngmy/vim-rubocop'
Plug 'nightsense/snow'
Plug 'plytophogy/vim-virtualenv'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'szw/vim-maximizer'
Plug 'szw/vim-tags'
Plug 'terryma/vim-multiple-cursors'
Plug 'tomtom/tcomment_vim'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-markdown'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-scripts/taglist.vim'
Plug 'w0rp/ale'
Plug 'will133/vim-dirdiff'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins'  }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
call plug#end()
" }}}

" FZF {{{
" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }
" Default fzf layout
let g:fzf_layout = { 'down': '~40%' }
" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
let g:fzf_history_dir = '~/.local/share/fzf-history'
let g:fzf_buffers_jump = 1
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
let g:fzf_tags_command = 'ctags -R --exclude=.git --exclude=log'
let g:fzf_commands_expect = 'alt-enter,ctrl-x'
nnoremap <silent> <Leader><Leader> :Files<CR>
nnoremap <silent> <Leader>. :Buffers<CR>
nnoremap <silent> <expr> <Leader><Leader> (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":Files\<cr>"
nnoremap <silent> <Leader>C  :Colors<CR>
nnoremap <silent> <Leader>i  :Lines<CR>
nnoremap <silent> <Leader>ag :Ag <C-R><C-W><CR>
nnoremap <silent> <Leader>rg :Rg <C-R><C-W><CR>
nnoremap <silent> <Leader>AG :Ag <C-R><C-A><CR>
xnoremap <silent> <Leader>ag y:Ag <C-R>"<CR>
nnoremap <silent> <Leader>m  :Marks<CR>
autocmd! FileType fzf
autocmd  FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler" }}}
let $FZF_DEFAULT_COMMAND = 'ag -g ""'
" }}}

" Ripgrep {{{
nnoremap <leader>a :Ag
if executable('rg')
  set grepprg=rg\
  let g:ackprg = 'rg %s --files --glob ""'
endif
set wildignore+=*/.git/*,*/tmp/*,*.swp
" }}}

" BufferLine {{{
let g:bufferline_echo = 0
" }}}

" Airline {{{
set laststatus=2
" let g:airline_theme = 'alduin'
let g:airline_theme = 'snow_dark'
let g:airline_powerline_fonts = 1
" }}}

" Remappings {{{
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <leader>l :set list!<CR>
nnoremap gd g<C-]>
noremap <C-C> <Esc>
noremap <silent> <F6> :let @+=expand("%:r")<CR>
map <Leader>b Oimport pdb; pdb.set_trace()  # BREAKPOINT<C-c>
map <silent> <F4> :syntax sync fromstart<CR>
" }}}

" Newtr {{{
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 0
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:netrw_hide = 1
let g:netrw_list_hide = netrw_gitignore#Hide()
let g:netrw_dirhistmax = 0
" }}}

" UndoTree {{{
nnoremap <leader>u :UndotreeToggle<CR>
set hidden
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000
let g:undotree_WindowLayout = 3
let g:undotree_DiffAutoOpen = 0
" }}}

" TagList {{{
let Tlist_Close_On_Select=1
let Tlist_File_Fold_Auto_Close=1
let Tlist_GainFocus_On_ToggleOpen=1
let Tlist_WinWidth=50
let Tlist_Use_Right_Window=1
nnoremap <leader>q :TagbarToggle<CR>
" }}}

" Colors {{{
syntax enable           " enable syntax processing
set background=dark
colorscheme seoul256
" colorscheme snow
if &term =~ '256color'
    set t_ut=
endif
" }}}

" Black {{{
autocmd BufWritePre *.py execute ':Black'
autocmd BufWritePre *.py execute ':Isort'
" }}}

" ALE {{{
let g:airline#extensions#ale#enabled = 1
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%] [%...code...%]'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_lint_on_text_changed = 'never'
let g:ale_set_highlights = 1
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 0
let g:ale_sign_error = '✖'
let g:ale_sign_warning = '▲'
let g:ale_linters = {
\   'javascript': ['prettier', 'eslint'],
\   'python': ['flake8', 'mypy'],
\}
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['prettier', 'eslint'],
\   'css': ['prettier'],
\}
let g:ale_fix_on_save = 1

" To allow JSX files to be read
augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
augroup END
" }}}

" Git gutter {{{
let g:gitgutter_sign_added = '✚'
let g:gitgutter_sign_modified = '●'
let g:gitgutter_sign_removed = '━'
let g:gitgutter_sign_removed_first_line = '━'
let g:gitgutter_sign_modified_removed = '┅'
" }}}

" Goyo + Limelight {{{
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
" }}}

" IncSearch {{{
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map z/ <Plug>(incsearch-fuzzy-/)
map z? <Plug>(incsearch-fuzzy-?)
map zg/ <Plug>(incsearch-fuzzy-stay)
function! s:config_easyfuzzymotion(...) abort
  return extend(copy({
  \   'converters': [incsearch#config#fuzzy#converter()],
  \   'modules': [incsearch#config#easymotion#module()],
  \   'keymap': {"\<CR>": '<Over>(easymotion)'},
  \   'is_expr': 0,
  \   'is_stay': 1
  \ }), get(a:, 1, {}))
endfunction

" noremap <silent><expr> <Space>/ incsearch#go(<SID>config_easyfuzzymotion())
" }}}

" EasyMotion {{{
map <Leader>w <Plug>(easymotion-prefix)

" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>W <Plug>(easymotion-bd-w)
nmap <Leader>W <Plug>(easymotion-overwin-w)
" }}}

" Etc. {{{
autocmd BufWritePre * %s/\s\+$//e
filetype indent on
filetype plugin on
set autoread
set noshowmode
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
let g:markdown_minlines = 80
set formatoptions+=j
let g:deoplete#enable_at_startup = 1

" let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'
" }}}

" Jedi {{{
" Disable autocompletion (using deoplete instead)
if has('nvim')
  let g:jedi#completions_enabled = 0
else
  let g:jedi#completions_enabled = 1
endif

" All these mappings work only for python code:
" Go to definition
let g:jedi#goto_definitions_command = '<leader>d'
" Find ocurrences
let g:jedi#usages_command = '<leader>o'
" Find assignments
let g:jedi#goto_assignments_command = '<leader>g'
let g:jedi#goto_definitions_command = "<leader>d"
" }}}
"
let g:pad#dir = '~/.notable/notes'
