ZSH=$HOME/.oh-my-zsh

ZSH_THEME="cmder"

CASE_SENSITIVE="true"
DISABLE_AUTO_TITLE="false"
ENABLE_CORRECTION="false"

plugins=(git nvm virtualenvwrapper tmux autojump rvm django thefuck)

source $ZSH/oh-my-zsh.sh

if command -v tmux>/dev/null; then
  [[ ! $TERM =~ screen ]] && [ -z $TMUX ] && exec tmux new-session -A -s default
fi

# Plain alias
alias da="deactivate"
alias reload="source ~/.zshrc"
alias nopyc="find . -name '*.pyc' | xargs rm -f || true"
alias ll="ls -la"
alias djmanage="python manage.py "
alias dc="docker-compose"
alias vim="nvim"

export LC_ALL=$LANG
export EDITOR="vim"
export SSH_KEY_PATH="~/.ssh/rsa_id"
export VIRTUALENVWRAPPER_PYTHON="/usr/bin/python"
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

removecontainers() {
    docker stop $(docker ps -aq)
    docker rm $(docker ps -aq)
}

armageddon() {
    removecontainers
    docker network prune -f
    docker rmi -f $(docker images --filter dangling=true -qa)
    docker volume rm $(docker volume ls --filter dangling=true -q)
    docker rmi -f $(docker images -qa)
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

eval $(thefuck --alias)
eval "$(pyenv init -)"
